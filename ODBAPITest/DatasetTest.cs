﻿using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ODBAPITest.Model;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace ODBAPITest
{
    [TestClass]
    public class DatasetTest
    {
        public DatasetTest()
        {
            _restClient = new RestClient(Properties.Resources.APIPath);
        }

        private readonly RestClient _restClient;

        [TestMethod]
        public void Test_01_DatasetListOK()
        {
            var request = new RestRequest("dataset", DataFormat.Json).AddHeader("key", Properties.Resources.APIKey).AddHeader("page","1");
            var response = _restClient.Get<DatasetPage>(request);
            Assert.IsTrue(response.StatusCode ==  HttpStatusCode.OK,
                $"Wrong HTTP code. Expected: {HttpStatusCode.OK} Got: {response.StatusCode}");
            Assert.IsNotNull(response?.Data,
                "Response data was NULL");
            Assert.IsTrue(response.Data.Datasets.Count > 0,
                "Number of datasets is NOT > 0");
            int itemsOnPage = Math.Min(response.Data.ItemsCount, response.Data.ItemsPerPage);
            Assert.AreEqual(itemsOnPage, response.Data.Datasets.Count,
                $"Number of datasets on page is wrong. Expected: {itemsOnPage} Got: {response.Data.Datasets.Count}");
            
            if (response.Data.Pages == 1)
                return;
            request = new RestRequest("dataset", DataFormat.Json).AddHeader("key", Properties.Resources.APIKey).AddHeader("page", response.Data.Pages.ToString());
            response = _restClient.Get<DatasetPage>(request);
            int totalItems = response.Data.ItemsPerPage * (response.Data.Pages - 1) + response.Data.Datasets.Count;
            Assert.AreEqual(response.Data.ItemsCount, totalItems, 
                $"Wrong number of total datasets. Expected: {response.Data.ItemsCount} Got: {totalItems}");
        }

        [TestMethod]
        public void Test_02_DatasetListIdBadType()
        {
            var request = new RestRequest("dataset", DataFormat.Json).AddHeader("key", Properties.Resources.APIKey).AddHeader("page", "qwasd");
            var response = _restClient.Get<List<DatasetBasic>>(request);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.BadRequest,
                $"Wrong HTTP code. Expected: {HttpStatusCode.BadRequest} Got: {response.StatusCode}");
        }

        [TestMethod]
        public void Test_03_DatasetListIdNotExist()
        {
            var request = new RestRequest("dataset", DataFormat.Json).AddHeader("key", Properties.Resources.APIKey).AddHeader("page", "9852");
            var response = _restClient.Get<List<DatasetBasic>>(request);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.NotFound,
                $"Wrong HTTP code. Expected: {HttpStatusCode.NotFound} Got: {response.StatusCode}");
        }

        [TestMethod]
        public void Test_04_DatasetId()
        {
            var request = new RestRequest("dataset/11", DataFormat.Json).AddHeader("key", Properties.Resources.APIKey);
            var response = _restClient.Get<Dataset>(request);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK,
                $"Wrong HTTP code. Expected: {HttpStatusCode.OK} Got: {response.StatusCode}");
            Assert.IsNotNull(response?.Data,
                "Response data was NULL");
            Assert.AreEqual(response.Data.UniqId, "a0xhdebg18deib23snqy", true,
                $"Received dataset with wrong uniqid. Expected: a0xhdebg18deib23snqy Got: {response.Data.UniqId}");
        }

        [TestMethod]
        public void Test_05_DatasetIdBadType()
        {
            var request = new RestRequest("dataset/qweasd", DataFormat.Json).AddHeader("key", Properties.Resources.APIKey);
            var response = _restClient.Get<Dataset>(request);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.BadRequest,
                $"Wrong HTTP code. Expected: {HttpStatusCode.BadRequest} Got: {response.StatusCode}");
        }

        [TestMethod]
        public void Test_06_DatasetIdNotExist()
        {
            var request = new RestRequest("dataset/95182", DataFormat.Json).AddHeader("key", Properties.Resources.APIKey);
            var response = _restClient.Get<Dataset>(request);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.NotFound,
                $"Wrong HTTP code. Expected: {HttpStatusCode.NotFound} Got: {response.StatusCode}");
        }

        [TestMethod]
        public void Test_07_DatasetIdFiles()
        {
            var request = new RestRequest("dataset/11/files", DataFormat.Json).AddHeader("key", Properties.Resources.APIKey);
            var response = _restClient.Get<FileBasicList>(request);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK,
                $"Wrong HTTP code. Expected: {HttpStatusCode.OK} Got: {response.StatusCode}");
            Assert.IsNotNull(response?.Data?.Files,
                "Response data was NULL");
            Assert.IsTrue(response.Data.Files.Count > 0,
                "Response length was NOT > 0");

            FileBasic fb = response.Data.Files.SingleOrDefault(fb => fb.Id == 1015);

            Assert.IsNotNull(fb,
                "File with id 1015 was not found in dataset 11");

        }

    }
}
