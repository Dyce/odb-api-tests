﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;
using RestSharp.Extensions;
using System;
using System.IO;
using System.Net;
using System.Reflection;

namespace ODBAPITest
{
    [TestClass]
    public class FileTest
    {
        public FileTest()
        {
            _restClient = new RestClient(Properties.Resources.APIPath);
        }

        private readonly RestClient _restClient;

        [TestMethod]
        public void Test_01_FileIdOK()
        {
            var request = new RestRequest("file/1015", DataFormat.Json).AddHeader("key", Properties.Resources.APIKey);
            var response = _restClient.Get<Model.File>(request);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK,
                $"Wrong HTTP code. Expected: {HttpStatusCode.OK} Got: {response.StatusCode}");
            Assert.IsNotNull(response?.Data,
                "Response data was NULL");
        }

        [TestMethod]
        public void Test_02_FileIdBadType()
        {
            var request = new RestRequest("file/qweasd", DataFormat.Json).AddHeader("key", Properties.Resources.APIKey);
            var response = _restClient.Get<Model.File>(request);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.BadRequest,
                $"Wrong HTTP code. Expected: {HttpStatusCode.BadRequest} Got: {response.StatusCode}");
        }

        [TestMethod]
        public void Test_03_FileIdNotFound()
        {
            var request = new RestRequest("file/987752", DataFormat.Json).AddHeader("key", Properties.Resources.APIKey);
            var response = _restClient.Get<Model.File>(request);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.NotFound,
                $"Wrong HTTP code. Expected: {HttpStatusCode.NotFound} Got: {response.StatusCode}");
        }

        [TestMethod]
        [Obsolete]
        public void Test_04_FileDownloadOK()
        {
            var request = new RestRequest("file/1015", DataFormat.Json).AddHeader("key", Properties.Resources.APIKey);
            var response = _restClient.Get<Model.File>(request);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK,
                $"Wrong HTTP code. Expected: {HttpStatusCode.OK} Got: {response.StatusCode}");
            request = new RestRequest("file/1015/download", DataFormat.None).AddHeader("key", Properties.Resources.APIKey);
            if(System.IO.File.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"testfile.{response.Data.Type}")))
            {
                System.IO.File.Delete(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"testfile.{response.Data.Type}"));
            }
            _restClient.DownloadData(request).SaveAs(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"testfile.{response.Data.Type}"));
            Assert.IsTrue(System.IO.File.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"testfile.{response.Data.Type}")), 
                "Failed to download file");
        }
    }
}
