﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ODBAPITest.Model;
using RestSharp;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection.Metadata;

namespace ODBAPITest
{
    [TestClass]
    public class CategoryTest
    {
        public CategoryTest()
        {
            _restClient = new RestClient(Properties.Resources.APIPath);
        }

        private readonly RestClient _restClient;

        [TestMethod]
        public void Test_01_CategoryDefault()
        {
            var request = new RestRequest("category", DataFormat.Json).AddHeader("key", Properties.Resources.APIKey);
            var response = _restClient.Get<CategoryResponse>(request);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK,
                $"Wrong HTTP code. Expected: {HttpStatusCode.OK} Got: {response.StatusCode}");
            Assert.IsNotNull(response?.Data?.Categories, 
                "Response data was NULL");
            Assert.IsTrue(response.Data.Categories.Count>0, 
                "Response length was NOT > 0");

            Category category = response.Data.Categories.SingleOrDefault(c => c.Id == 1);

            Assert.IsNotNull(category,
                "No category with id 1 found");
            Assert.AreEqual(category.Name, "Doprava", true,
                $"Category has different name. Expected: Doprava Got: {category.Name}");
        }

        [TestMethod]
        public void Test_02_CategoryLocaleSK()
        {
            var request = new RestRequest("category", DataFormat.Json).AddHeader("key", Properties.Resources.APIKey).AddHeader("locale", "sk");
            var response = _restClient.Get<CategoryResponse>(request);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK,
                $"Wrong HTTP code. Expected: {HttpStatusCode.OK} Got: {response.StatusCode}");
            Assert.IsNotNull(response?.Data?.Categories,
                "Response data was NULL");
            Assert.IsTrue(response.Data.Categories.Count > 0,
                "Response length was NOT > 0");

            Category category = response.Data.Categories.SingleOrDefault(c => c.Id == 1);

            Assert.IsNotNull(category,
                "No category with id 1 found");
            Assert.AreEqual(category.Name, "Doprava", true,
                $"Category has different name. Expected: Doprava Got: {category.Name}");
        }

        [TestMethod]
        public void Test_03_CategoryLocaleEN()
        {
            var request = new RestRequest("category", DataFormat.Json).AddHeader("key", Properties.Resources.APIKey).AddHeader("locale", "en");
            var response = _restClient.Get<CategoryResponse>(request);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK,
                $"Wrong HTTP code. Expected: {HttpStatusCode.OK} Got: {response.StatusCode}");
            Assert.IsNotNull(response?.Data?.Categories,
                "Response data was NULL");
            Assert.IsTrue(response.Data.Categories.Count > 0,
                "Response length was NOT > 0");

            Category category = response.Data.Categories.SingleOrDefault(c => c.Id == 1);

            Assert.IsNotNull(category,
                "No category with id 1 found");
            Assert.AreEqual(category.Name, "Transport", true,
                $"Category has different name. Expected: Transport Got: {category.Name}");
        }

        [TestMethod]
        public void Test_04_CategoryLocaleWrong()
        {
            var request = new RestRequest("category", DataFormat.Json).AddHeader("key", Properties.Resources.APIKey).AddHeader("locale", "jp");
            var response = _restClient.Get<CategoryResponse>(request);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK,
                $"Wrong HTTP code. Expected: {HttpStatusCode.OK} Got: {response.StatusCode}");
            Assert.IsNotNull(response?.Data?.Categories,
                "Response data was NULL");
            Assert.IsTrue(response.Data.Categories.Count > 0,
                "Response length was NOT > 0");

            Category category = response.Data.Categories.SingleOrDefault(c => c.Id == 1);

            Assert.IsNotNull(category,
                "No category with id 1 found");
            Assert.AreEqual(category.Name, "Doprava", true,
                $"Category has different name. Expected: Doprava Got: {category.Name}");
        }

        [TestMethod]
        public void Test_05_CategoryIdOK()
        {
            var request = new RestRequest("category/1", DataFormat.Json).AddHeader("key", Properties.Resources.APIKey);
            var response = _restClient.Get<Category>(request);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK,
                $"Wrong HTTP code. Expected: {HttpStatusCode.OK} Got: {response.StatusCode}");
            Assert.IsNotNull(response?.Data,
                "Response data was NULL");

            Category category = response.Data;

            Assert.IsNotNull(category,
                "No category with id 1 found");
            Assert.AreEqual(category.Name, "Doprava", true,
                $"Category has different name. Expected: Doprava Got: {category.Name}");
        }

        [TestMethod]
        public void Test_06_CategoryIdBadType()
        {
            var request = new RestRequest("category/qwe", DataFormat.Json).AddHeader("key", Properties.Resources.APIKey);
            var response = _restClient.Get<CategoryTest>(request);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.BadRequest,
                $"Wrong HTTP code. Expected: {HttpStatusCode.BadRequest} Got: {response.StatusCode}");
        }

        [TestMethod]
        public void Test_07_CategoryIdNotExists()
        {
            var request = new RestRequest("category/98547", DataFormat.Json).AddHeader("key", Properties.Resources.APIKey);
            var response = _restClient.Get<CategoryTest>(request);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.NotFound,
                $"Wrong HTTP code. Expected: {HttpStatusCode.NotFound} Got: {response.StatusCode}");
        }

        [TestMethod]
        public void Test_08_CategoryIdDatasetsOK()
        {
            var request = new RestRequest("category/5/datasets", DataFormat.Json).AddHeader("key", Properties.Resources.APIKey);
            var response = _restClient.Get<DatasetBasicList>(request);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK,
                $"Wrong HTTP code. Expected: {HttpStatusCode.OK} Got: {response.StatusCode}");
            Assert.IsNotNull(response?.Data,
                "Response data was NULL");
            Assert.IsTrue(response.Data.Datasets.Count> 0,
                "Response length was NOT > 0");

            DatasetBasic dsb = response.Data.Datasets.SingleOrDefault(d => d.Id == 11);

            Assert.IsNotNull(dsb?.Slug,
                "No dataset with specified id was found");
            Assert.AreEqual(dsb.Slug, "obyvpocet", true, 
                $"Dataset has wrong slug. Expected: obyvpocet Got: ${dsb.Slug}");
        }
    }
}
