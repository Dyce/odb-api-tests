using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;
using System.Net;
using System.Reflection.Metadata;

namespace ODBAPITest
{
    [TestClass]
    public class AuthTest
    {
        public AuthTest()
        {
            _restClient = new RestClient(Properties.Resources.APIPath);
        }

        private readonly RestClient _restClient;
        
        [TestMethod]
        public void Test_01_NoAPIKey()
        {
            var request = new RestRequest("category", DataFormat.Json);
            var response = _restClient.Get(request);
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.Unauthorized, 
                $"Wrong HTTP code. Expected: {HttpStatusCode.Unauthorized} Got: {response.StatusCode}");
        }

        [TestMethod]
        public void Test_02_WrongAPIKey()
        {
            var request = new RestRequest("category", DataFormat.Json).AddHeader("key", "wrongvalue");
            var response = _restClient.Get(request);
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.Unauthorized,
                $"Wrong HTTP code. Expected: {HttpStatusCode.Unauthorized} Got: {response.StatusCode}");
        }

        [TestMethod]
        public void Test_03_CorrectAPIKey()
        {
            var request = new RestRequest("category", DataFormat.Json).AddHeader("key", Properties.Resources.APIKey);
            var response = _restClient.Get(request);
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.OK,
                $"Wrong HTTP code. Expected: {HttpStatusCode.OK} Got: {response.StatusCode}");
        }
    }
}
