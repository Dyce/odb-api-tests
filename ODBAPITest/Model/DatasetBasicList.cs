﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ODBAPITest.Model
{
    public class DatasetBasicList
    {
        public List<DatasetBasic> Datasets { get; set; }
    }
}
