﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ODBAPITest.Model
{
    public class Dataset
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
        public string CreatedAt { get; set; }
        public string ChangedAt { get; set; }
        public string Licence { get; set; }
        public int Category { get; set; }
        public int Year { get; set; }
        public string District { get; set; }
        public string UniqId { get; set; }
        public int Downloaded { get; set; }
        public string PowerBI { get; set; }
        public string Map { get; set; }
        public bool OnlineData { get; set; }
        public List<Tag> Tags { get; set; }
    }
}
