﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ODBAPITest.Model
{
    public class File
    {
        public int Id { get; set; }
        public int Dataset { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string CreatedAt { get; set; }
        public string PowerBI { get; set; }
        public string Map { get; set; }
    }
}
