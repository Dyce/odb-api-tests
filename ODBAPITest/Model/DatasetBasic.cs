﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ODBAPITest.Model
{
    public class DatasetBasic
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }

    }
}
