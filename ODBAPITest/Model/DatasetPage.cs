﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ODBAPITest.Model
{
    public class DatasetPage
    {
        public int Page { get; set; }
        public int Pages { get; set; }
        public int ItemsCount { get; set; }
        public int ItemsPerPage { get; set; }
        public List<DatasetBasic> Datasets { get; set; }
    }
}
